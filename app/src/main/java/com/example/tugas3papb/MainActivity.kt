package com.example.tugas3papb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.CheckBox
import android.widget.ImageView


class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val kumisCheck =findViewById<CheckBox>(R.id.kumisCheck)
        val alisCheck =findViewById<CheckBox>(R.id.alisCheck)
        val rambutCheck =findViewById<CheckBox>(R.id.rambutCheck)
        val janggutCheck =findViewById<CheckBox>(R.id.janggutCheck)

        val ivRambut = findViewById<View>(R.id.ivRambut)
        val ivAlis = findViewById<ImageView>(R.id.ivAlis)
        val ivKumis = findViewById<View>(R.id.ivKumis)
        val ivJanggut = findViewById<ImageView>(R.id.ivJanggut)

        //Kumis Listener
        kumisCheck.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                ivKumis.visibility = View.VISIBLE
            } else {
                ivKumis.visibility = View.INVISIBLE
            }
        }

        //Alis Listener
        alisCheck.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                ivAlis.visibility = View.VISIBLE
                //jika kondisi Checkbox tercentang maka visibility set to VISIBLE
            } else {
                ivAlis.visibility = View.INVISIBLE
            }
        }

        //Rambut Listener
        rambutCheck.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                ivRambut.visibility = View.VISIBLE
            } else {
                ivRambut.visibility = View.INVISIBLE
            }
        }

        //Janggut Listener
        janggutCheck.setOnClickListener { view ->
            if ((view as CheckBox).isChecked) {
                ivJanggut.visibility = View.VISIBLE
            } else {
                ivJanggut.visibility = View.INVISIBLE
            }
        }
    }





}